import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import LandingLogin from './views/Login.vue'
import LandingRegister from './views/Register.vue'
import LandingHome from './views/Home.vue'

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LandingHome',
      component: LandingHome,
    },
    {
      path: '/login',
      name: 'LandingLogin',
      component: LandingLogin,
    },
    {
      path: '/sign-up',
      name: 'LandingRegister',
      component: LandingRegister,
    },
  ]
})
