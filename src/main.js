import axios from 'axios'
import join from 'url-join'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import AuthService from './services/Auth';

Vue.config.productionTip = false

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['Content-Type'] = 'application/json'

const isAbsoluteURLRegex = /^(?:\w+:)\/\//
const authService = new AuthService();

/**
 * Set base url
 */
axios.interceptors.request.use((config) => {
  // Concatenate base path if not an absolute URL
  if (!isAbsoluteURLRegex.test(config.url)) {
    config.url = join(process.env.VUE_APP_BASE_URI, config.url);
  }

  return config;
});

/**
 * Set Authorization token to Request
 */
axios.interceptors.request.use((config) => {
  if (authService.isLoggedIn()) {
    config.headers.common['Authorization'] = 'Bearer ' + authService.getToken();
  }

  return config;
});


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
